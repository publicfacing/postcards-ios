//
//  PFAHomeViewController.m
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/2/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import "PFAHomeViewController.h"

@interface PFAHomeViewController ()
@property (nonatomic, strong) UIImageView *ipadHomeBgImageView;
@end

#define BORDER 15

@implementation PFAHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIFont *buttonFont = [UIFont fontWithName:@"League Gothic" size:24];
    
    // google anaylitics
    self.trackedViewName = @"Home Screen";
    
    if (isPad)
    {
        self.ipadHomeBgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_bg_ipad.jpg"]];
        [self.borderBackgroundImageView addSubview:self.ipadHomeBgImageView];
    }
    
    // font setup
    [self.createCardButton.titleLabel setFont:buttonFont];
    [self.saveCardsButton.titleLabel setFont:buttonFont];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.60 green:0.16 blue:0.06 alpha:1.0];
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    CGRect screenBound = screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenSize = screenBound.size;

    //position elements for iphone 5
	if (screenSize.height > 480 && isPhone)
    {
        self.backgroundImage.image = [UIImage imageNamed:@"home_bg-568h@2.jpg"];
        CGRect newFrame = self.createCardButton.frame;
        CGRect saveFrame = self.saveCardsButton.frame;
        newFrame.origin.x = 60;
        saveFrame.origin.x = 298;
        
        self.createCardButton.frame = newFrame;
        self.saveCardsButton.frame = saveFrame;
    }
    
    if (isPad)
    {
        if (isPortrait)
        self.ipadHomeBgImageView.frame = CGRectMake(BORDER, BORDER, screenSize.width - (BORDER * 2), screenSize.height - (BORDER * 2) - 20);
        else
        self.ipadHomeBgImageView.frame = CGRectMake(BORDER, BORDER, screenSize.height - (BORDER * 2), screenSize.width - (BORDER * 2) - 20);
    }
    
}

- (void)pushNotificationAlert:(NSString *)message
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)newCardButtonPressed:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"editorViewSegue" sender:nil];
}

- (IBAction)savedCardButtonPressed:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"savedViewSegue" sender:nil];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

@end
