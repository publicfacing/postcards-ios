//
//  PFAArtistGalleryScrollViewController.m
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/4/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import "PFAArtistGalleryScrollViewController.h"
#import "PFAArtistGallerySharedStore.h"
#import "PFAArtistImage.h"
#import "PFAEditorViewController.h"
#import "PFAOutlineText.h"

@interface PFAArtistGalleryScrollViewController ()
{
    CGRect screenBound;
    CGSize screenSize;
}
@property (nonatomic, strong) NSArray *gallery;
@property (nonatomic, setter = isImageSelectable:) BOOL imageSelectable;
@end

@implementation PFAArtistGalleryScrollViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	screenBound = screenBound = [[UIScreen mainScreen] bounds];
    screenSize = screenSize = screenBound.size;
    
    // google anaylitics
    self.trackedViewName = @"Artist Gallery Screen";
    
    UIBarButtonItem *selectButton = [[UIBarButtonItem alloc] initWithTitle:@"Select" style:UIBarButtonItemStyleBordered target:self action:@selector(selectButtonPressed)];
    self.navigationItem.rightBarButtonItem = selectButton;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:tap];
    
    // load all the images from the gallery and add them to the scroll view
    
    self.navigationItem.title = self.category;
    self.gallery = [[NSArray alloc] initWithArray:[[[PFAArtistGallerySharedStore sharedStore] galleries] objectForKey:self.category]];
    
    for (int i = 1 ; i <= [self.gallery count] ; i++)
    {
 
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        imageView.frame = CGRectMake(0, 0, screenSize.height, screenSize.width - 40);
        
        PFAArtistImage *image = [self.gallery objectAtIndex:i - 1];
        
        NSString *path = image.path;
        
        if (isRetina)
        {
            path = [path stringByReplacingOccurrencesOfString:@".jpg" withString:@"@2x.jpg"];
        }
        
        UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        activityView.center = imageView.center;
        
        [imageView addSubview:activityView];
        
        [activityView startAnimating];
        
        [imageView setImageWithURL:[NSURL URLWithString:path] placeholderImage:[UIImage imageNamed:@"placeholder-artistgallery.png"] options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            [activityView stopAnimating];
            [selectButton setEnabled:YES];
            [self isImageSelectable:YES];
        }];
        
        imageView.tag = i;
        
        // add caption to image
        PFAOutlineText *caption = [[PFAOutlineText alloc] initWithFrame:CGRectMake(0, imageView.frame.size.height - 50, imageView.frame.size.width, 50)];
        [caption setBackgroundColor:[UIColor clearColor]];
        [caption setFont:[UIFont boldSystemFontOfSize:16.0f]];
        [caption setTextColor:[UIColor whiteColor]];
        [caption setTextAlignment:NSTextAlignmentCenter];
        [caption setText:image.caption];
        [imageView addSubview:caption];
        
        [self.artistGalleryScrollView addSubview:imageView];
        
        if (activityView.isAnimating)
        {
            [self isImageSelectable:NO];
            [selectButton setEnabled:NO];
        }
    }
	
    [self layoutScrollImages:[self.gallery count]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleSingleTap:(UITapGestureRecognizer *)gr
{
    if (self.imageSelectable)
        [self selectButtonPressed];
}

- (void)layoutScrollImages :(int)numberOfImages
{
	UIImageView *view = nil;
	NSArray *subviews = [self.artistGalleryScrollView subviews];
    
	// reposition all image subviews in a horizontal serial fashion
	CGFloat curXLoc = 0;
	for (view in subviews)
	{
		if ([view isKindOfClass:[UIImageView class]] && view.tag > 0)
		{
			CGRect frame = view.frame;
			frame.origin = CGPointMake(curXLoc, 0);
			view.frame = frame;
			
			curXLoc += (screenSize.height);
		}
	}
	
	// set the content size so it can be scrollable
	[self.artistGalleryScrollView setContentSize:CGSizeMake((numberOfImages * screenSize.height), [self.artistGalleryScrollView bounds].size.width - 40)];
    
    // scroll to selected index
    [self.artistGalleryScrollView setContentOffset:CGPointMake((self.artistGalleryScrollView.frame.size.height * self.selectedIndex), self.artistGalleryScrollView.frame.origin.y)];
}

- (void)selectButtonPressed
{
    
    // send image from imageview
    
    int selectedIndex = self.artistGalleryScrollView.contentOffset.x / self.artistGalleryScrollView.bounds.size.width;
    
    NSArray *subViews = [self.artistGalleryScrollView subviews];
    UIImageView *imageview = [subViews objectAtIndex:selectedIndex];
    
    PFAArtistImage *image = [self.gallery objectAtIndex:selectedIndex];
    
    PFAEditorViewController *editorViewController = [self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count] - 4];
    
    PFAPostCard *postcard = editorViewController.postcard;
    
    [postcard isArtistImage:YES];
    [postcard setArtistName:image.artistName];
    [postcard setArtistPath:image.artistPath];
    [postcard setCaptionName:image.caption];
    [postcard setImage:imageview.image];
    [editorViewController setPostcard:postcard];
    [editorViewController.postcardBackImageView setImage:[editorViewController setupImageForPostcardBack:imageview.image]];
    
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count] - 4] animated:YES];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

@end
