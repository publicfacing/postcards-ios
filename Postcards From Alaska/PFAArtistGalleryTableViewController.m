//
//  PFAArtistGalleryTableViewController.m
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/3/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import "PFAArtistGalleryTableViewController.h"
#import "PFAArtistGallerySharedStore.h"
#import "PFAArtistGalleryCollectionViewController.h"

@interface PFAArtistGalleryTableViewController ()

@end

@implementation PFAArtistGalleryTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = NO;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *path = self.tableView.indexPathForSelectedRow;
    [segue.destinationViewController setCategory:[[[self.tableView cellForRowAtIndexPath:path] textLabel] text]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[[PFAArtistGallerySharedStore sharedStore] categories] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"artistGalleryTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [[cell textLabel] setText:[[[PFAArtistGallerySharedStore sharedStore] categories] objectAtIndex:[indexPath row]]];
    
    return cell;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

@end
