//
//  PFAMessageViewController.m
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/5/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import "PFAMessageViewController.h"
#import "PFAEditorViewController.h"
#import "PFAPostCard.h"

@interface PFAMessageViewController ()
@property (nonatomic, strong) PFAEditorViewController *editorViewController;
@end

@implementation PFAMessageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // google anaylitics
    self.trackedViewName = @"Message Input Screen";

    self.editorViewController = [self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count] - 2];
    
    self.postcard = self.editorViewController.postcard;
    
    [self.messageTextView setText:self.postcard.message];
    
    [self.messageTextView becomeFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneButtonPressed:(UIBarButtonItem *)sender
{
    
    if (![self.messageTextView.text isEqualToString:@""])
    {
        [self.postcard setMessage:self.messageTextView.text];
        [self.editorViewController setPostcard:self.postcard];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

@end
