//
//  PFAEditorViewController.m
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/2/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import "PFAEditorViewController.h"
#import "util.h"
#import "PFAArtistGalleryCollectionViewController.h"
#import "PFASender.h"
#import "PFARecipient.h"
#import "PFAPostCardStore.h"
#import "PFAMessageViewController.h"
#import "PFAPurchaseViewController.h"
#import "GAI.h"

#define BORDER_SIZE 7

@interface PFAEditorViewController ()
@property (nonatomic, setter = isChangeImageViewOpen:) BOOL changeImageViewOpen;
@property (nonatomic, setter = isFirstViewAppear:) BOOL firstViewAppear;
@property (nonatomic, setter = isCardFlipped:) BOOL cardFlipped;
@property (nonatomic, strong) NSMutableParagraphStyle *paragraphStyle;
@property (nonatomic, strong) NSDictionary *attributtes;
@property (nonatomic, strong) UIImage *flipFront;
@property (nonatomic, strong) UIImage *flipBack;;
@property (nonatomic, copy) PFAPostCard *savedPostcard;
@property (nonatomic) CGRect addressFrame;
@end

@implementation PFAEditorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    [self isFirstViewAppear:YES];
    
    [self isCardFlipped:NO];
    
    if (!self.isSavedCard)
        self.postcard = [[PFAPostCard alloc] init];
    
    // google anaylitics
    self.trackedViewName = @"Editor Screen";
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"uiAction"
                                                    withAction:@"buttonPress"
                                                     withLabel:self.artistGalleryButton.titleLabel.text
                                                     withValue:[NSNumber numberWithInt:100]];
    
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"uiAction"
                                                    withAction:@"buttonPress"
                                                     withLabel:self.takePictureButton.titleLabel.text
                                                     withValue:[NSNumber numberWithInt:100]];
    
    [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"uiAction"
                                                    withAction:@"buttonPress"
                                                     withLabel:self.userGalleryButton.titleLabel.text
                                                     withValue:[NSNumber numberWithInt:100]];
    
    // card flip button
    self.flipFront = [UIImage imageNamed:@"front.png"];
    self.flipBack = [UIImage imageNamed:@"back.png"];
    
    // add tapgesture
    self.singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:self.singleTap];
    
    // set border radius of messageview
    [self.messageTextView.layer setCornerRadius:5.0f];
    
    // have to add postcardImageBack manually so the return image will scale properly
    self.postcardBackImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"placeholder.png"]];
    [self.postcardBackImageView setContentMode:UIViewContentModeScaleAspectFill];
    [self.postcardBackView addSubview:self.postcardBackImageView];
    
    // setup line spacing for address field
    self.paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    self.paragraphStyle.minimumLineHeight = 17.f;
    self.paragraphStyle.maximumLineHeight = 17.f;
    self.attributtes = @{NSParagraphStyleAttributeName : self.paragraphStyle,};
        
    // setup saved card
    if (self.isSavedCard)
    {
        PFAPostCardStore *ps = [PFAPostCardStore sharedStore];
        NSArray *postcards = [ps allPostcards];
        self.savedPostcard = [postcards objectAtIndex:self.savedCardIndex];
        [self setPostcard:self.savedPostcard];
        [self.postcardBackImageView setImage:[self setupImageForPostcardBack:self.postcard.image]];
    }
        
    // setup border view
    self.borderView = [[UIView alloc] initWithFrame:self.postcardBackView.frame];
    [self.borderView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [self.borderView.layer setBorderWidth:BORDER_SIZE];
    [self.postcardBackView addSubview:self.borderView];
    
    // create frame from address labels
    self.addressFrame = CGRectMake(self.addressNameLabel.frame.origin.x + self.postcardView.frame.origin.x - 20, self.addressCountryLabel.frame.origin.y + self.postcardView.frame.origin.y + self.addressCountryLabel.frame.size.height, self.addressCountryLabel.frame.size.width + 40, self.addressNameLabel.frame.origin.y - self.addressNameLabel.frame.origin.x);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // center change image popup
    if (isPhone)
    self.changeImagePopupView.frame = CGRectMake((self.view.frame.size.width / 2) - (self.changeImagePopupView.frame.size.width / 2) - 20, (self.view.frame.size.height / 2) - (self.changeImagePopupView.frame.size.height / 2), self.changeImagePopupView.frame.size.width, self.changeImagePopupView.frame.size.height);
    
    // change image popup if time view is shown
    if ([self firstViewAppear])
    {
        if (![self isSavedCard])
        [self changeImage];
        
        [self isFirstViewAppear:NO];
    }

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
    // update text fields
    if (self.postcard.message != NULL)
    {
        [self.messageTextView setText:self.postcard.message];
    }
    
    if (self.postcard.sender.name != NULL)
    {
        [self.fromLabel setText:self.postcard.sender.name];
    }
    
    [self.captionLabel setText:self.postcard.caption];
    
    // rotate caption
    self.captionLabel.transform = CGAffineTransformMakeRotation(-M_PI_2);
    
    // set address labels
    if (self.postcard.recipient.name != NULL)
    {        
        [self.addressNameLabel setText:self.postcard.recipient.name];
        [self.addressStreetLabel setText:[NSString stringWithFormat:@"%@ %@", self.postcard.recipient.address1, self.postcard.recipient.address2]];
        [self.addressCityStateZipLabel setText:[NSString stringWithFormat:@"%@ %@ %@", self.postcard.recipient.city, self.postcard.recipient.state, self.postcard.recipient.zip]];
        [self.addressCountryLabel setText:self.postcard.recipient.country];
    }
    
    // button font
    UIFont *buttonFont = [UIFont fontWithName:@"League Gothic" size:24];
    [self.takePictureButton.titleLabel setFont:buttonFont];
    [self.userGalleryButton.titleLabel setFont:buttonFont];
    [self.artistGalleryButton.titleLabel setFont:buttonFont];
    
    self.changeImagePopupView.layer.borderWidth = 1.0f;
    self.changeImagePopupView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    // set border based on postcard
    if (![self.postcard hasBorder])
    {
        // change icon image
        [self.borderButton setImage:[UIImage imageNamed:@"add_border.png"] forState:UIControlStateNormal];
        
        // remove border
        [self.borderView setHidden:YES];
    }
    else
    {
        [self.borderButton setImage:[UIImage imageNamed:@"remove_border.png"] forState:UIControlStateNormal];
        
        // add border
        [self.borderView setHidden:NO];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews
{ 
    [super viewDidLayoutSubviews];
    CGRect screenBound = screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenSize = screenBound.size;
    
    //position elements for iphone 5
	if (screenSize.height > 480 && isPhone)
    {
        self.postcardView.frame = CGRectMake(54, self.postcardView.frame.origin.y, self.postcardView.frame.size.width, self.postcardView.frame.size.height);
    }
}

- (void)handleSingleTap:(UITapGestureRecognizer *)gr
{
    
    CGPoint location = [gr locationInView:self.postcardView];
    
    if (self.cardFlipped)
    {    
        if (CGRectContainsPoint(self.messageTextView.frame, location))
        {
            [self performSegueWithIdentifier:@"messageViewSegue" sender:@"messageView"];
        }
        
        if (CGRectContainsPoint(self.addressFrame, location))
        {
            [self performSegueWithIdentifier:@"addressViewSegue" sender:nil];
        }
        
        if (CGRectContainsPoint(self.fromLabel.frame, location))
        {
            [self performSegueWithIdentifier:@"fromNameSegue" sender:nil];
        }
    }
    else
    {
        if (CGRectContainsPoint(self.postcardBackView.frame, location))
        {
            if (![self changeImageViewOpen])
            [self changeImage];
        }
    }
}

- (void)flipPostcard
{
    if (!self.cardFlipped)
    {
        
        [UIView transitionWithView:self.postcardView
                          duration:1
                           options:UIViewAnimationOptionTransitionFlipFromRight
                        animations:^{
                            
                [self.postcardBackView setHidden:YES];
                [self.postcardFrontView setHidden:NO];
                
                [self performSelector:@selector(switchButtons) withObject:self afterDelay:0.5];
                
                [self isCardFlipped:YES];
                
                            
        }completion:nil];
        
    }
    else
    {

        [UIView transitionWithView:self.postcardView
                          duration:1
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            
                [self.postcardBackView setHidden:NO];
                [self.postcardFrontView setHidden:YES];
                
                [self performSelector:@selector(switchButtons) withObject:self afterDelay:0.5];
                
                [self isCardFlipped:NO];
                            
        }completion:nil];
        
    }

}

- (void)switchButtons
{
    
    if (self.cardFlipped)
    {  
        [self.changeImageButton setHidden:YES];
        [self.captionButton setHidden:NO];
        [self.borderButton setHidden:YES];
        [self.sendButton setHidden:NO];
        [self.flipButton setImage:self.flipFront forState:UIControlStateNormal];
    }
    else
    {
        [self.changeImageButton setHidden:NO];
        [self.captionButton setHidden:YES];
        [self.borderButton setHidden:NO];
        [self.sendButton setHidden:YES];
        [self.flipButton setImage:self.flipBack forState:UIControlStateNormal];
    }
}

- (void)changeImage
{
    self.changeImagePopupView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.grayBackground setHidden:NO];
        [self.changeImagePopupView setHidden:NO];
        self.changeImagePopupView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
    }];
    
    [self isChangeImageViewOpen:YES];
}

- (UIView *)changeImagePopupView
{
    if (!_changeImagePopupView)
    {
        _changeImagePopupView = [self.view viewWithTag:200];
    }
    
    return _changeImagePopupView;
}

- (UIImagePickerController *)imagePicker
{
    if (!_imagePicker)
    {
        _imagePicker = [[UIImagePickerController alloc] init];
    }
    
    return _imagePicker;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);

    [[self postcard] setImage:image];
    
    [self.postcard isArtistImage:NO];
    
    [self.postcardBackImageView setImage:[self setupImageForPostcardBack:image]];

    image = nil;
     
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (UIImage *)setupImageForPostcardBack :(UIImage *)image
{
    
    /**
     *  This mess fixes images taken with the camera not rotating
     */
    
    // find the proportion scale of the image
    float scale = 1.0;
    
    if (image.size.width < image.size.height)
        scale = image.size.width / image.size.height;
    else
        scale = image.size.height / image.size.width;
        
    // set max size to 420 and scale other side
    CGSize landscapeScale = CGSizeMake(420, scale * 420);
    CGSize portraitScale = CGSizeMake(scale * 420, 420);
    
    if (isPad)
    {
        landscapeScale = CGSizeMake(970, 642);
        portraitScale = CGSizeMake(642, 970);
    }

    if (isRetina)
    {
        landscapeScale = CGSizeMake(landscapeScale.width * 2, landscapeScale.height * 2);
        portraitScale = CGSizeMake(portraitScale.width * 2, portraitScale.height * 2);
    }
    
    UIImage *resizeImage = nil;
    
    // warn if image is low resolution
    if (image.size.width < 1800 && image.size.height < 1800 && ![self.postcard artistImage])
    {
        NSLog(@"Image Width: %f, Image Height: %f", image.size.width, image.size.height);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Low Resolution Detected" message:@"This image may be too small to print" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    if (image.size.width < image.size.height)
    {
        UIImage *portraitImage = [util imageWithImage:image scaledToSize:portraitScale];
        resizeImage = [[UIImage alloc] initWithCGImage:portraitImage.CGImage scale:1.0 orientation:UIImageOrientationRight];
        portraitImage = nil;
    }
    else
    {
        resizeImage = [util imageWithImage:image scaledToSize:landscapeScale];
    }
    
    return resizeImage;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == self.saveCardAlert)
    {
        if (buttonIndex == 1)
        {
            [[PFAPostCardStore sharedStore] addPostcard:self.postcard];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
        if (buttonIndex == 2)
        {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
        [self.saveCardAlert dismissWithClickedButtonIndex:0 animated:YES];
    }
    
    if (alertView == self.updateCardAlert)
    {
        if (buttonIndex == 1)
        {
            [[PFAPostCardStore sharedStore] replacePostcardAtIndex:self.savedCardIndex withPostcard:self.postcard];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
        if (buttonIndex == 2)
        {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
        [self.saveCardAlert dismissWithClickedButtonIndex:0 animated:YES];
    }
    
    if (alertView == self.noCaptionAllowedAlert)
    {
        [self.noCaptionAllowedAlert dismissWithClickedButtonIndex:0 animated:YES];  
    }
    
    if (alertView == self.noCameraAlert)
    {
        [self.noCaptionAllowedAlert dismissWithClickedButtonIndex:0 animated:YES];
    }
}

- (void)popChangeImagePopupView
{
    self.changeImagePopupView.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.changeImagePopupView setHidden:YES];
        [self.grayBackground setHidden:YES];
        self.changeImagePopupView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
    }];
    [self isChangeImageViewOpen:NO];
}

- (BOOL)checkForCompletePostcard
{
    if (![self.postcard hasImage])
    {
        [self alertIncompleteCardWithMessage:@"Image"];
        [self flipPostcard];
        return NO;
    }
    else if (![self.postcard hasMessage])
    {
        [self alertIncompleteCardWithMessage:@"Message"];
        return NO;
    }
    else if (![self.postcard hasRecipient])
    {
        [self alertIncompleteCardWithMessage:@"Recipient"];
        return NO;
    }
    else if (![self.postcard hasSender])
    {
        [self alertIncompleteCardWithMessage:@"From"];
        return NO;
    }
    
    return YES;
}

- (void)alertIncompleteCardWithMessage :(NSString *)message
{
    self.cardCheckAlert = [[UIAlertView alloc]  initWithTitle:@"Missing Information"
                                                      message:[NSString stringWithFormat:@"%@ information is missing", message]
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [self.cardCheckAlert show];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isEqualToString:@"purchaseView"])
    {
        PFAPurchaseViewController *pvc = segue.destinationViewController;
        [pvc setPostcard:self.postcard];
        
        if (self.isSavedCard)
        {
            [pvc setIsSavedCard:YES];
            [pvc setSavedCardIndex:self.savedCardIndex];
        }
        else
        {
            [pvc setIsSavedCard:NO];
        }
    }
}



- (IBAction)closeButtonPressed:(UIButton *)sender
{

    [self popChangeImagePopupView];
    
}

- (IBAction)borderButtonPressed:(UIButton *)sender
{
    
    if ([self.postcard hasBorder])
    {
        // change icon image
        [self.borderButton setImage:[UIImage imageNamed:@"add_border.png"] forState:UIControlStateNormal];
        
        // remove border
        [self.borderView setHidden:YES];
        [self.postcard doesHaveBorder:NO];
    }
    else
    {
        [self.borderButton setImage:[UIImage imageNamed:@"remove_border.png"] forState:UIControlStateNormal];
        [self.postcard doesHaveBorder:YES];
        
        // add border
        [self.borderView setHidden:NO];
    }
    
}

- (IBAction)changeImageButtonPressed:(UIButton *)sender
{
    [self changeImage];
}

- (IBAction)takePictureButtonPressed:(UIButton *)sender
{
    // check for camera
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        [self.imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    else {
        self.noCameraAlert = [[UIAlertView alloc] initWithTitle:@"No Camera"
                                                        message:@"you must have a camera in order to take a picture"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [self.noCameraAlert show];
    }
    
    [self.imagePicker setDelegate:self];
    
    //place image picker on screen
    [self popChangeImagePopupView];
    [self presentViewController:self.imagePicker animated:YES completion:nil];
    
}

- (IBAction)userGalleryButtonPressed:(UIButton *)sender
{
    [self.imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self.imagePicker setDelegate:self];
    [self presentViewController:self.imagePicker animated:YES completion:nil];
    [self popChangeImagePopupView];
}

- (IBAction)homeButtonPressed:(UIButton *)sender
{
    self.saveCardAlert = [[UIAlertView alloc] initWithTitle:@"Would you like to save this postcard?"
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Yes", @"No", nil];
    
    self.updateCardAlert = [[UIAlertView alloc] initWithTitle:@"Would you like to update this postcard?"
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Yes", @"No", nil];
    if (self.isSavedCard)
        [self.updateCardAlert show];
    else
        [self.saveCardAlert show];
}

- (IBAction)artistGalleryButtonPressed:(UIButton *)sender
{
    PFAArtistGalleryCollectionViewController *acvc = [self.storyboard instantiateViewControllerWithIdentifier:@"artistGalleryTableView"];
    [self popChangeImagePopupView];
    [self.navigationController pushViewController:acvc animated:YES];
}

- (IBAction)flipCardButton:(UIButton *)sender
{
    [self flipPostcard];
}

- (IBAction)captionButtonPressed:(UIButton *)sender
{
    if (![self.postcard artistImage])
    {
        [self performSegueWithIdentifier:@"captionViewSegue" sender:nil];
    }
    else
    {
        self.noCaptionAllowedAlert = [[UIAlertView alloc] initWithTitle:@"Artist Gallery Photo"
                                                                message:@"You can only add captions to your photos"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
        [self.noCaptionAllowedAlert show];
    }
}

- (IBAction)purchaseButtonPressed:(UIButton *)sender
{
    if ([self checkForCompletePostcard])
    {
        [self performSegueWithIdentifier:@"purchaseViewSegue" sender:@"purchaseView"];
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

@end
