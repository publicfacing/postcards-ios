//
//  PFASender.h
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/2/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PFASender : NSObject <NSCoding>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;

@end
