//
//  PFAMessageViewController.h
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/5/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PFAPostCard.h"
#import "GAITrackedViewController.h"

@interface PFAMessageViewController : GAITrackedViewController

@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (nonatomic, strong) PFAPostCard *postcard;

- (IBAction)doneButtonPressed:(UIBarButtonItem *)sender;

@end
