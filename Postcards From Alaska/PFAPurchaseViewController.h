//
//  PFAPurchaseViewController.h
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/4/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@class PFAPostCard;

@interface PFAPurchaseViewController : GAITrackedViewController <UIAlertViewDelegate, NSURLConnectionDataDelegate>

@property (weak, nonatomic) IBOutlet UIButton *purchaseButton;
@property (weak, nonatomic) IBOutlet UIButton *promoButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *warningLabel;

@property (nonatomic, strong) PFAPostCard *postcard;
@property (nonatomic) BOOL isSavedCard;
@property (nonatomic) NSInteger savedCardIndex;

@property (nonatomic, strong) UIAlertView *promoCodeAlertView;

- (IBAction)promoButtonPressed:(UIButton *)sender;
- (IBAction)cancelButtonPressed:(UIButton *)sender;
- (IBAction)purchaseButtonPressed:(UIButton *)sender;

@end
