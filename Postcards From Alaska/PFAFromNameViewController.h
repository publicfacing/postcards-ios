//
//  PFAFromNameViewController.h
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/5/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface PFAFromNameViewController : GAITrackedViewController

@property (weak, nonatomic) IBOutlet UITextField *fromTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

- (IBAction)doneButtonPressed:(id)sender;

@end
