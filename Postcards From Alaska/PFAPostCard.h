//
//  PFAPostCard.h
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/2/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PFARecipient, PFASender;

@interface PFAPostCard : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *caption;
@property (nonatomic, strong) NSString *artistPath;
@property (nonatomic, strong) NSString *yearString;
@property (nonatomic, strong) NSString *captionName;
@property (nonatomic, strong) NSString *captionLocation;
@property (nonatomic, strong) NSString *artistName;

@property (nonatomic) float price;

@property (nonatomic, strong) PFARecipient *recipient;
@property (nonatomic, strong) PFASender *sender;

@property (nonatomic, setter = isArtistImage:) BOOL artistImage;
@property (nonatomic, setter = doesHaveBorder:) BOOL hasBorder;
@property (nonatomic, setter = doesHaveMessage:) BOOL hasMessage;
@property (nonatomic, setter = doesHaveRecipient:) BOOL hasRecipient;
@property (nonatomic, setter = doesHaveSender:) BOOL hasSender;
@property (nonatomic, setter = doesHaveImage:) BOOL hasImage;

@end
