//
//  PFAArtistGalleryCollectionViewController.h
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/3/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface PFAArtistGalleryCollectionViewController : UICollectionViewController

@property (nonatomic, strong) NSString *category;

@end
