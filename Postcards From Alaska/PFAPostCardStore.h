//
//  PFAPostCardStore.h
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/10/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PFAPostCard;

@interface PFAPostCardStore : NSObject

+(PFAPostCardStore *)sharedStore;

- (NSArray *)allPostcards;

- (void)addPostcard :(PFAPostCard *)postcard;
- (void)deletePostcard :(PFAPostCard *)postcard;
- (void)replacePostcardAtIndex :(NSUInteger)index withPostcard:(PFAPostCard *)postcard;

- (NSString *)postcardArchivePath;
- (BOOL)saveChanges;

@end
