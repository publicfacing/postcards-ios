//
//  PFAArtistGallerySharedStore.h
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/3/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PFAArtistGallerySharedStore : NSObject

@property (nonatomic, strong) NSMutableArray *categories;
@property (nonatomic, strong) NSMutableDictionary *galleries;

+ (PFAArtistGallerySharedStore *)sharedStore;

@end
