//
//  PFAAddressViewController.h
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/5/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBookUI/AddressBookUI.h>
#import "GAITrackedViewController.h"

@interface PFAAddressViewController : GAITrackedViewController <UITextFieldDelegate, ABPeoplePickerNavigationControllerDelegate, ABNewPersonViewControllerDelegate, ABUnknownPersonViewControllerDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *toTextField;
@property (weak, nonatomic) IBOutlet UITextField *address1TextField;
@property (weak, nonatomic) IBOutlet UITextField *address2TextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *stateTextField;
@property (weak, nonatomic) IBOutlet UITextField *countryTextField;
@property (weak, nonatomic) IBOutlet UITextField *zipTextField;

@property (nonatomic, strong) UITapGestureRecognizer *singleTap;

@property (nonatomic, strong) NSString *countryFromTableView;

@property (weak, nonatomic) UIButton *contactsBarButton;
@property (weak, nonatomic) UIButton *addContactsBarButton;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)doneButtonPressed:(UIBarButtonItem *)sender;
- (void)contactsBarButtonPressed;

@end
