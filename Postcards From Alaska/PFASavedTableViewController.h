//
//  PFASavedTableViewController.h
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/10/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFASavedTableViewController : UITableViewController

- (IBAction)toggleEditMode:(UIBarButtonItem *)sender;

@end
