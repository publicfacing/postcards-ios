//
//  PFANavigationController.h
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/8/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFANavigationController : UINavigationController

@end
