//
//  PFAArtistGalleryScrollViewController.h
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/4/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "GAITrackedViewController.h"

@interface PFAArtistGalleryScrollViewController : GAITrackedViewController <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *artistGalleryScrollView;
@property (nonatomic) int selectedIndex;
@property (nonatomic, strong) NSString *category;

@end
