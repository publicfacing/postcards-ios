//
//  PFAPostCardStore.m
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/10/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import "PFAPostCardStore.h"
#import "PFAPostCard.h"

@interface PFAPostCardStore()
{
    NSMutableArray *allPostcards;
}
@end

@implementation PFAPostCardStore

- (id)init
{
    self = [super init];
    if (self)
    {
        NSString *path = self.postcardArchivePath;
        allPostcards = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
        
        if (!allPostcards)
            allPostcards = [[NSMutableArray alloc] init];
    }
    return self;
}

+ (PFAPostCardStore *)sharedStore
{
    static PFAPostCardStore *sharedStore = nil;
    if (!sharedStore)
        sharedStore = [[super allocWithZone:nil] init];
    
    return sharedStore;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedStore];
}

- (NSArray *)allPostcards
{
    return allPostcards;
}

- (void)addPostcard :(PFAPostCard *)postcard
{
    if (postcard)
        [allPostcards addObject:postcard];
}

- (void)deletePostcard:(PFAPostCard *)postcard
{
    [allPostcards removeObjectIdenticalTo:postcard];
}

- (void)replacePostcardAtIndex:(NSUInteger)index withPostcard:(PFAPostCard *)postcard
{
    [allPostcards replaceObjectAtIndex:index withObject:postcard];
}

- (NSString *)postcardArchivePath
{
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    // get only document directory
    NSString *documentDirectory = [documentDirectories objectAtIndex:0];
    
    return [documentDirectory stringByAppendingPathComponent:@"postcards.archive"];
}

- (BOOL)saveChanges
{
    // returns success or failure
    NSString *path = self.postcardArchivePath;
    return [NSKeyedArchiver archiveRootObject:allPostcards toFile:path];
}

@end
