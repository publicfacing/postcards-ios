//
//  PFAPostCard.m
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/2/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import "PFAPostCard.h"
#import "PFASender.h"
#import "PFARecipient.h"

@implementation PFAPostCard

- (id)init
{
    self = [super init];
    if (self)
    {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy"];
        
        if (!self.yearString)
            self.yearString = [formatter stringFromDate:[NSDate date]];
        self.recipient = [[PFARecipient alloc] init];
        self.sender = [[PFASender alloc] init];
        [self setPrice:1.49];
        self.image = [[UIImage alloc] init];
        [self doesHaveRecipient:NO];
        [self doesHaveSender:NO];
        [self doesHaveImage:NO];
        [self doesHaveBorder:YES];
        [self isArtistImage:NO];
        [self doesHaveMessage:NO];
    }

    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self)
    {
        
        [self setArtistPath:[aDecoder decodeObjectForKey:@"artistPath"]];
        [self doesHaveBorder:[aDecoder decodeBoolForKey:@"hasBorder"]];
        [self isArtistImage:[aDecoder decodeBoolForKey:@"isArtistImage"]];
        [self setArtistName:[aDecoder decodeObjectForKey:@"artistName"]];
        [self setMessage:[aDecoder decodeObjectForKey:@"message"]];
        [self setCaption:[aDecoder decodeObjectForKey:@"caption"]];
        [self setRecipient:[aDecoder decodeObjectForKey:@"recipient"]];
        [self setSender:[aDecoder decodeObjectForKey:@"sender"]];
        [self setImage:[aDecoder decodeObjectForKey:@"image"]];
        [self setCaptionName:[aDecoder decodeObjectForKey:@"captionName"]];
        [self setCaptionLocation:[aDecoder decodeObjectForKey:@"captionLocation"]];
        
    }
    
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    PFAPostCard *newPostcard = [[[self class] alloc] init];
    
    [newPostcard isArtistImage:[self artistImage]];
    [newPostcard setArtistPath:[self artistPath]];
    [newPostcard setArtistName:[self artistName]];
    [newPostcard setMessage:[self message]];
    [newPostcard setCaption:[self caption]];
    [newPostcard setRecipient:[self recipient]];
    [newPostcard setSender:[self sender]];
    [newPostcard setImage:[self image]];
    [newPostcard setCaptionName:[self captionName]];
    [newPostcard setCaptionLocation:[self captionLocation]];
    [newPostcard doesHaveBorder:[self hasBorder]];
     
     return newPostcard;
}

- (void)isArtistImage:(BOOL)artistImage
{
    _artistImage = artistImage;
    
    if (!artistImage)
    {
        self.artistPath = nil;
        [self setPrice:1.49];
    }
    else
    {
        [self doesHaveImage:YES];
        [self setPrice:1.99];
    }
}

- (void)setMessage:(NSString *)message
{
    _message = message;
    
    [self doesHaveMessage:YES];
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    [self doesHaveImage:YES];
}

- (void)setRecipient:(PFARecipient *)recipient
{
    _recipient = recipient;
    
    if (self.recipient.name)
        [self doesHaveRecipient:YES];
}

- (void)setCaptionName:(NSString *)captionName
{
    _captionName = captionName;
    
    if ([_captionName isEqualToString:@""])
        [self setCaptionName:nil]; 
}

- (void)setCaptionLocation:(NSString *)captionLocation
{
    _captionLocation = captionLocation;
    
    if ([_captionLocation isEqualToString:@""])
        [self setCaptionLocation:nil];
}

- (NSString *)caption
{
    if (!self.captionName && !self.captionLocation)
        [self setCaption:[NSString stringWithFormat:@"photo \xC2\xA9%@", self.yearString]];
    else if (self.captionName && !self.captionLocation)
        [self setCaption:[NSString stringWithFormat:@"photo \xC2\xA9%@ %@", self.yearString, self.captionName]];
    else if (!self.captionName && self.captionLocation)
        [self setCaption:[NSString stringWithFormat:@"photo \xC2\xA9%@ %@", self.yearString, self.captionLocation]];
    else
        [self setCaption:[NSString stringWithFormat:@"photo \xC2\xA9%@ %@, %@", self.yearString, self.captionName, self.captionLocation]];
    
    return _caption;
}

- (void)setSender:(PFASender *)sender
{
    _sender = sender;
    
    if (self.sender.name != nil)
    [self doesHaveSender:YES];
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.artistName forKey:@"artistName"];
    [aCoder encodeObject:self.message forKey:@"message"];
    [aCoder encodeObject:self.artistPath forKey:@"artistPath"];
    [aCoder encodeObject:self.image forKey:@"image"];
    [aCoder encodeObject:self.sender forKey:@"sender"];
    [aCoder encodeObject:self.recipient forKey:@"recipient"];
    [aCoder encodeObject:self.caption forKey:@"caption"];
    [aCoder encodeObject:self.captionName forKey:@"captionName"];
    [aCoder encodeObject:self.captionLocation forKey:@"captionLocation"];
    [aCoder encodeBool:self.artistImage forKey:@"isArtistImage"];
    [aCoder encodeBool:self.hasBorder forKey:@"hasBorder"];
}

@end
