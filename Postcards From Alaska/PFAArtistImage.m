//
//  PFAArtistImage.m
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/3/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import "PFAArtistImage.h"


@implementation PFAArtistImage

- (id)init
{
    self = [super init];
    
    return self;
}

- (id)initWithPath :(NSString *)path thumbPath:(NSString *)thumbPath caption:(NSString *)caption artistName:(NSString *)artistName
{
    self = [super init];
    if (self)
    {
        self.path = path;
        self.thumbPath = thumbPath;
        self.caption = caption;
        self.artistPath = [path stringByReplacingOccurrencesOfString:@"https://postcardsfromalaska.com/ntrnl/postcard_data/images/artist/" withString:@""];
        self.artistName = artistName;
    }
    
    return self;
}

@end
