//
//  PFAOutlineText.h
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/24/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFAOutlineText : UILabel

@end
