//
//  PFACountryTableViewController.h
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/7/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFACountryTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *countries;
@property (nonatomic, strong) NSString *selectedCountry;

@end
