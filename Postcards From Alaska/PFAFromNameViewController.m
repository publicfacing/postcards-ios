//
//  PFAFromNameViewController.m
//  Postcards From Alaska
//
//  Created by Keeano Martin on 5/5/13.
//  Copyright (c) 2013 Keeano Martin. All rights reserved.
//

#import "PFAFromNameViewController.h"
#import "PFAEditorViewController.h"
#import "PFASender.h"

@interface PFAFromNameViewController ()
@property (nonatomic, strong) PFAEditorViewController *editorViewController;
@end

@implementation PFAFromNameViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // google anaylitics
    self.trackedViewName = @"From Input Screen";
    
    self.editorViewController = [self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count] - 2];
    
    [self.fromTextField setText:self.editorViewController.postcard.sender.name];
    [self.emailTextField setText:self.editorViewController.postcard.sender.email];
    
    [self.fromTextField becomeFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneButtonPressed:(UIBarButtonItem *)sender
{
    
    PFASender *cardSender = [[PFASender alloc] init];
    
    [cardSender setName:self.fromTextField.text];
    [cardSender setEmail:self.emailTextField.text];
    
    [self.editorViewController.postcard setSender:cardSender];
    
    if ([self.fromTextField.text isEqualToString:@""])
        [self.editorViewController.postcard doesHaveSender:NO];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

@end
